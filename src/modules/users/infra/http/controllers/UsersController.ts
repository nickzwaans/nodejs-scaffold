import * as usersRepository from "@modules/users/model/repositories/UsersRepository";

export async function getOne(): Promise<any> {
    return await usersRepository.getOne();
}
