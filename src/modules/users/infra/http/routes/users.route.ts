import {RouteOptions} from "fastify";
import * as usersController from '@modules/users/infra/http/controllers/UsersController'

const users: RouteOptions = {
    method:  'GET',
    url:     '/users',
    handler: usersController.getOne(),
}
