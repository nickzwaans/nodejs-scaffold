import fastify, {RouteOptions} from "fastify";
import {sharedRoutes} from "./infrastructure/http/routes/index.route";

const main = (): void => {
    const server = fastify();

    sharedRoutes.forEach((route: RouteOptions) => server.route(route));
    console.info(`\nIndexed the following shared routes: \n${(sharedRoutes.map(route => route.url)).join('\n')}\n`)

    server.listen(process.env.PORT, (err: any, address: any) => {
        if (err) {
            console.error(err)
            process.exit(1)
        }
        console.log(`Server listening at ${address}`)
    })
}

main();
