import { RouteOptions } from 'fastify';

//todo: Do we use DI?
const defaultRoute: RouteOptions = {
    method:  'GET',
    url:     '/',
    handler: async () => {
        console.info('Incoming request on ping.')
        return 'Pong';
    }
}

const ping: RouteOptions = {
    method:  'GET',
    url:     '/ping',
    handler: async () => {
        console.info('Incoming request on ping.')
        return 'Pong';
    }
}

export const sharedRoutes: Array<RouteOptions> = [
    ping,
    defaultRoute
]
