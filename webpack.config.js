/* eslint-disable */
const path = require('path');
const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');
const tsPaths = require('tsconfig-paths-webpack-plugin');
const tsChecker = require('fork-ts-checker-webpack-plugin');

module.exports = {
    mode: 'production',
    entry: 'src/main.ts',
    devtool: 'inline-source-map',
    target: 'node',
    externals: [ nodeExternals() ],
    resolve: {
        extensions: ['.js', '.json', '.ts'],
        plugins: [ new tsPaths() ],
    },
    output: {
        libraryTarget: 'commonjs',
        path: path.join(__dirname, 'dist'),
        filename: '[name].js',
    },
    plugins: [
    ],
    module: {
        rules: [
            { test: /\.ts$/, loader: 'ts-loader', options: { transpileOnly: true } },
        ],
    },
};
