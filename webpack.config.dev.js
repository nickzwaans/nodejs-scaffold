/* eslint-disable */
const NodemonPlugin = require('nodemon-webpack-plugin');
const path = require('path');
const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');
const tsPaths = require('tsconfig-paths-webpack-plugin');
const tsChecker = require('fork-ts-checker-webpack-plugin');
const Dotenv = require('dotenv-webpack');

module.exports = {
    mode: 'development',
    entry: 'src/main.ts',
    devtool: 'inline-source-map',
    target: 'node',
    externals: [nodeExternals()],
    watchOptions: {
        aggregateTimeout: 600,
        ignored: 'node_modules',
    },
    resolve: {
        extensions: ['.js', '.json', '.ts'],
        plugins: [new tsPaths()],
    },
    output: {
        libraryTarget: 'commonjs',
        path: path.join(__dirname, 'dist'),
        filename: 'main.js',
    },
    plugins: [
        new tsChecker(),
        new NodemonPlugin(),
        new Dotenv()
    ],
    module: {
        rules: [
            {
                test: /\.ts$/,
                loader: 'ts-loader',
                options: {transpileOnly: true}
            },
        ],
    },
};
